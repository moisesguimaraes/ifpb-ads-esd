# Estrutura de Dados

* **Curso:** [Análise e Desenvolvimento de Sistemas - IFPB Cajazeiras](http://www.ifpb.edu.br/campi/cajazeiras/cursos/cursos-superiores-de-tecnologia/analise-e-desenvolvimento-de-sistemas)
* **Professor:** Moisés Guimarães
* **Precisando de Ajuda?**
    * Consulte esta página ou crie uma  [issue](https://bitbucket.org/moisesguimaraes/ifpb-ads-esd/issues).
    * [Email](mailto:moises.medeiros@ifpb.edu.br) para dúvidas pontuais, ou para marcar uma reunião.

## Descrição da Disciplina

Aprenda como criar e utilizar estruturas de dados. Tópicos da disciplina incluem: Recursividade; Conceitos e técnicas de manipulação de vetores, listas encadeadas, filas, pilhas, árvores e grafos; Algoritmos de ordenação, pesquisa e tabelas de dispersão. Desenvolva em uma das linguagens de programação mais populares do mercado, a linguagem C.

Computadores serão disponibilizados nos laboratórios, apesar de que os alunos são incentivados a trazerem seus notebooks para eventuais exercícios em sala de aula.

## Pré-requisitos

* Fundamentos da Computação; Algoritmos e Lógica de Programação - ou equivalente.
* Compreensão de variáveis​​, tipos de dados, controle de fluxo e utilização de funções - veja Materiais para [Iniciantes](#markdown-header-materiais-para-iniciante).
* Conhecimento de linha de comando, variáveis de ambiente e sistemas de controle de versão são um plus.

Estes conceitos não serão cobrados pelo professor, mas você pode ficar bem perdido sem entende-los.

## Objetivo Geral

Capacitar o aluno para trabalhar com informação de forma estruturada, conhecer métodos de classificação e de pesquisa de dados entendendo suas aplicações. Vamos mergulhar nas nuances da linguagem C, como alocação de memória, tipos abstratos de dados, e como podem ser utilizados para construir estruturas de dados complexas. Ferramentas como linha de comando e git serão utilizadas. O foco será no desenvolvimento de aplicações acessíveis via linha de comando. Os tópicos abordados incluem:

### Objetivos Específicos

* Pensar de forma recursiva;
* Diferenciar as estruturas de dados básicas;
* Conhecer as operações sobre cada estrutura de dado;
* Conhecer os algoritmos de classificação de dados;
* Conhecer os algoritmos de pesquisa de dados;
* Compreender aplicações que façam uso de estruturas de dados;

O tópicos serão demonstrados através de codificação ao vivo durante a aula, exemplos e slides, disponíveis em [TODO](#). Exercícios adicionais poderão ser passados em sala de aula.

## Projetos

Todos os projetos estão listados no [Conteúdo Programático](#markdown-header-conteudo-programatico).

### Fluxo de trabalho

1. Duplique o repositório equivalente ao exercício/projeto com visibilidade privada (encontrado em [bitbucket.org/moisesguimaraes](https://bitbucket.org/moisesguimaraes));
1. Renomeie o repositório adicionando **-ano-semestre**:
    * e.g.: ifpb-ads-esd-fib -> ifpb-ads-esd-fib**-2014-1**;
1. Compartilhe o repositório duplicado com o professor;
1. Clone o repositório no seu computador;
1. Modifique os arquivos para completar a sua solução;
1. Execute ./test.sh (Unix) ou test.bat (Windows) para ver os resultados;
1. Garanta que todo o seu código foi comitado;
1. Envie o código para o seu repositório remoto no Bitbucket.

Para trabalhos com múltiplas entregas, crie tags entrega-1, entrega-2, ...

### Requisitos

Estes se aplicam na vida real também.

* Todo o código deve compilar sem erros ou warnings (utilize a flag -Wall do gcc para exibir todos os warnings);
* Devem ser aplicados "boas práticas de programação" aprendidas em sala:
    * Variáveis devem ser objetivas;
    * Validação de entrada de dados;
    * Retorno de erros adequados.
* Pontos bônus para:
    * Casos de testes adicionais;
    * Criatividade (Desde que cumpridos os requerimentos do projeto).

## Conteúdo Programático

### Semana 1

1. Introdução:
    * Instalando o git e o [SourceTree](http://www.sourcetreeapp.com/download/);
    * Cadastrando-se no Bitbucket.
1. Atribuições do aluno:
    * "Observar" este repositório.

        ![observar](https://bytebucket.org/moisesguimaraes/ifpb-ads-esd/raw/955ce3ef3492b94625002a42f7b9079d46e89f3c/assets/observar.png)

1. Fluxo de trabalho do Bitbucket
    * Veja [fluxo de trabalho](#markdown-header-fluxo-de-trabalho).
    
### Semana 2

1. Alocação de memória:
    * Leiam no livro C Completo e Total o capítulo 5 que fala sobre ponteiros.
    * Leiam [Mastering stack and heap](http://www.iar.com/Global/Resources/Developers_Toolbox/Building_and_debugging/Mastering_stack_and_heap_for_system_reliability.pdf).
    
1. Projeto Vector disponível [aqui](https://bitbucket.org/moisesguimaraes/ifpb-ads-esd-vector/overview):
    * Estutem a estrutura do projeto, nos próximos projetos o main não será entregue de bandeja.
    * Sigam o [fluxo de trabalho](#markdown-header-fluxo-de-trabalho) e criem uma tag **entrega-1** até a próxima aula.
    * **Atenção: apenas as entregas com tags realizadas antes da aula serão corrigidas!!!**
    
### Semana 3

1. Projeto Vector (entrega 2) disponível [aqui](https://bitbucket.org/moisesguimaraes/ifpb-ads-esd-vector/overview):
    * Alterem o código utilizando a função **realloc** para redimensionar o vetor de acordo com a seguinte regra:
        * Caso o vetor esteja cheio ao tentar inserir, redimensionar o vetor aumentando o seu tamanho em um bloco.
        * Caso haja duas vezes o tamanho do bloco de espaços livres no vetor após remover, redimensionar o mesmo diminuindo o seu tamanho em um bloco.
    * Sigam o [fluxo de trabalho](#markdown-header-fluxo-de-trabalho) e criem uma tag **entrega-2** até a próxima aula.
    * **Atenção: apenas as entregas com tags realizadas antes da aula serão corrigidas!!!**

## Dicas para trabalhos em equipe

* Grupos de três pessoas são possíveis, mas duplas trabalham melhor;
* Uma pessoa deve criar o repositório e compartilhar com os demais membros da equipe;
* Concordar com um editor e ambiente que todos estejam acostumados a utilizar;
* A pessoa que é menos experiente / confortável deve ter mais tempo de teclado; 
* Mudar quem "lidera" a equipe regularmente;
* Certifique-se de salvar o código e enviá-lo para os outros membros da equipe.

## Recursos

### Mais exemplos

* TODO

### Leitura Obrigatória

* C Completo e Total (Biblioteca do IFPB)

### Leitura Recomendada

* [Mastering stack and heap](http://www.iar.com/Global/Resources/Developers_Toolbox/Building_and_debugging/Mastering_stack_and_heap_for_system_reliability.pdf)

### Materiais para Iniciante

Esta disciplina assume que você tem conhecimento dos seguintes assuntos, mas no caso de você precisar de uma revisada...

* TODO

### Outros Materiais

* [Code School](http://www.codeschool.com/paths/javascript)
* [Teach Yourself to Code](http://teachyourselftocode.com/)

### Ferramentas

* perguntas: [Stack Overflow](http://pt.stackoverflow.com/)

### Bitbucket

* [Official GitHub Help](https://help.github.com/)
* [Recommended resources](https://help.github.com/articles/what-are-other-good-resources-for-learning-git-and-github)

## Avaliação

* Provas – 70%
* Projetos – 30%

## Considerações sobre plágio

### IFPB

> TODO

### Professor

Reuso e extensão de código são as principais partes do desenvolvimento de software moderno. Como um programador profissional você nunca vai escrever qualquer coisa a partir do zero. Esta disciplina porém incentiva a criação do zero para melhor aprendizado. Você é incentivado a aprender com a ajuda de seus colegas, mas não deve copiar o trabalho deles. Eu não vou caçar as pessoas que estão simplesmente copiando e colando soluções, porque sem desafiar a si mesmos, elas estão simplesmente desperdiçando seu tempo nesta disciplina, mas trabalhos descaradamente copiados serão zerados em ambas as partes (autor e plagiador), portanto mantenham seus trabalhos bem guardados.
