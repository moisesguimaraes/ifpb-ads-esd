#include <stdio.h>
#include <stdlib.h>

char* highstack = NULL;
char* lowstack  = NULL;

int fibonacci(int n) {
    if ((char*) &n < lowstack)
        lowstack = (char*) &n;
    
    if (n < 0)
        return 0;
    
    if (n == 0 || n == 1)
        return 1;
    
    return fibonacci(n - 1) + fibonacci(n - 2);
}

int main(int argc, char** argv) {
    highstack = (char*) &argc;
    lowstack  = (char*) &argv;
    
    if (argc > 1)
        fibonacci(atoi(argv[1]));

    printf("highstack = %p\n", highstack);
    printf("lowstack  = %p\n", lowstack);
    printf("consumo   = %lu\n", highstack - lowstack);

    return 0;
}