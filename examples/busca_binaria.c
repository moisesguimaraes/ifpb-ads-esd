#include <stdio.h>
#include <stdlib.h>

#define VECTOR_SIZE 10

int busca_a(int* v, int size, int e)
{
    int i = 0, f = size, m;
    
    while (f > i) {
        m = (i + f) / 2;
        
        if (v[m] == e)
            return m;
        
        if (v[m] > e)
            f = m;
        else
            i = m + 1;
    }
    
    return -1;
}

int busca_b(int* v, int n, int e)
{
    int m = n/2;
    
    if (!n)
        return -1;
    
    if (v[m] == e)
        return m;
    
    if (v[m] > e)
        return busca_b(v, m, e);
    
    return m + 1 + busca_b(v + m + 1, n - (m + 1), e);
}

int busca_c(int* v, int i, int f, int e)
{
    int m;
    
    if (f < i)
        return -1;
    
    m = (i + f) / 2;
    
    if (v[m] == e)
        return m;
    
    if (v[m] > e)
        return busca_c(v, i, m, e);
    
    return busca_c(v, m + 1, f, e);
}

int main (int argc, char** argv)
{
    int v[VECTOR_SIZE] = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9};
    int i;
    
    printf("BUSCA A\n");
    for (i = 0; i < VECTOR_SIZE; i++)
        printf("%d == %d\n", i, busca_a(v, VECTOR_SIZE, i));
    
    printf("BUSCA B\n");
    for (i = 0; i < VECTOR_SIZE; i++)
        printf("%d == %d\n", i, busca_b(v, VECTOR_SIZE, i));
    
    printf("BUSCA C\n");
    for (i = 0; i < VECTOR_SIZE; i++)
        printf("%d == %d\n", i, busca_c(v, 0, VECTOR_SIZE, i));
    
    return 0;
}