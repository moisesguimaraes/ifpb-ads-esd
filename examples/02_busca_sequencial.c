#include <stdio.h>
#include <stdlib.h>

#define SIZE 10
#define MAX(a, b) (((a) > (b)) ? (a) : (b))
#define MIN(a, b) (((a) < (b)) ? (a) : (b))

int Count(int* v, int size, int element)
{
    int i = 0;

    while(size--)
        if (v[size] == element)
            i++;

    return i;
}

int Find(int* v, int size, int element)
{
    int i;

    for (i = 0; i < size; i++)
        if (v[i] == element)
            return i;

    return -1;
}

void printv(int* v, int size)
{
    int i;

    printf("[");
    
    for (i = 0; i < size; i++)
        printf("%3d ", v[i]);

    printf(" ]\n");
}

int main(int argc, char** argv)
{
    int i;
    int x;
    int v[SIZE] = {0};
    int size = MIN(SIZE, argc - 1);

    for (i = 0; i < size; i++)
        v[i] = atoi(argv[i + 1]);
    
    printv(v, size);

    for (;;) {
        int pos = -1;
        int count;
        
        printf("\nx: ");
        scanf("%d", &x);

        if (x == 0)
            break;
        
        count = Count(v, size, x);
        printf("count = %d\n", count);

        while (count--) {
            pos = (pos + 1) + Find(v + (pos + 1), size - (pos + 1), x);
            printf("v[%d] = %d\n", pos, x);
        }
    }

    return 0;
}